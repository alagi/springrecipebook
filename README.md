# Programozás 4 projektfeladat
Varga Tamás - UE6LI3
PTE-MIK Mérnökinformatikus levelező
Alkalmazásfejleszői szakirány

### Spring Recipe Book
A projektfeladatomban egy receptkönyvet valósítok meg Spring Boot alapokon. A főbb eszközök, melyeket használtam:
- Spring Boot
- JPA&Hibernate
- H2 database
- Thymeleaf
- Spring Security
- Lombok
- Validation
- Tesztek

**Teszt felhasználók:**

- Username: **mgodisai**
- Password: test123
- is_mfa_enabled: true


- Username: **testadmin**
- Password: test123
- is_mfa_enabled: false


- Username: **testuser**
- Password: test123
- is_mfa_enabled: false

**Az alap működésen túl megvalósított opciók:**
- Custom Validation Messages
- Webjars használata
- Internationalization (Locale Resolver, Message Source) - három nyelvet (angol, magyar, német) kezel, de nincs teljes körűen megvalósítva (nem teljes körű jelenleg)
- 2-Factor-Authentication - alapszintű megvalósítás az Informatika Biztonság 2 tárgyhoz készített beadandóm gyakorlati alátámasztására (CustomAuthenticationProvider). [[https://drive.google.com/file/d/1jtfMtcApKWiSkCbDUvHfpbSyQzu37vPP/view?usp=sharing|MFA-OTP]]
    - Ez jelenleg egy felhasználóra (saját) van beállítva, mert az oldal jelenleg nem kezel regisztrációt, ahol a titkos kulcs generálásra és rögzítésre kerülhet. A saját felhasználó esetében manuálisan került rögzítésre a titkos kulcs, melyet az autentikátor applikációm is használ. Új felhasználó jelenleg nem hozható létre.
- A Security részben a felhasználói autentikáció mellett próbáltam egy role-based authorization megvalósítást is, de ki kellett kommentelnem, mert többszöri debug nekifutás ellenére működik.
- A web-es részben Thymeleaf template-kezelőt alkalmaztam, a mintapéldákhoz képest annyi kiegészítéssel, hogy az átirányítások után írok ki üzeneteket a RedirectAttributes segítségével. Elvileg ez a rész teljes körűen megvalósított, bár sok apróság még javítandó benne.

### Megvalósított Rest API végpontok
`GET /api/v1/recipe-categories` Kategórialista lekérése

`GET /api/v1/recipe-categories/{catId}` Egy adott kategória adatainak lekérése

`GET /api/v1/recipe-categories/{catId}/recipes` Egy adott kategóriához tartozó receptek lekérése

`POST /api/v1/recipe-categories` Új kategória rögzítése

`PUT /api/v1/recipe-categories/{catId}` Meglévő kategória frissítése, ha nem létező Id, akkor új kategóriát hoz létre

`PATCH /api/v1/recipe-categories/{catId}` Meglévő kategória frissítése, nem létező id esetén nem jön létre új kategória

`DELETE /api/v1/recipe-categories/{catId}` Megadott kategória törlése

`GET /api/v1/recipes` Az összes recept lekérése

`GET /api/v1/recipes/{recipeId}` Egy adott recept lekérése

`POST /api/v1/recipes` Új recept rögzítése

### Tesztek
A RecipeCategory entitásra vonatkozó rétegek tesztjei
- RestRecipeCategoryControllerTest
- RecipeCategoryServiceTest
- RecipeCategoryRepositoryTest