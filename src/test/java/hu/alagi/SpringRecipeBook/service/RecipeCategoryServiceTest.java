package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.repository.RecipeCategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
class RecipeCategoryServiceTest {

    @MockBean
    private RecipeCategoryRepository recipeCategoryRepository;

    @InjectMocks
    @Autowired
    private RecipeCategoryService recipeCategoryService;

    private RecipeCategory recipeCategory1;
    private RecipeCategory recipeCategory2;
    private List<RecipeCategory> recipeCategories;


    @BeforeEach
    void setUp() {
        this.recipeCategories=new ArrayList<>();
        recipeCategory1=RecipeCategory.builder()
                .name("Fagylaltok")
                .description("jeges finomságok")
                .build();
        recipeCategory2=RecipeCategory.builder()
                .name("Húsételek")
                .description(("húsból készült ételek"))
                .build();

        this.recipeCategories.add(recipeCategory1);
        this.recipeCategories.add(recipeCategory2);

    }

    @AfterEach
    void tearDown() {
        recipeCategory1=recipeCategory2=null;
        recipeCategories=null;

    }


    @Test
    public void getAllRecipeCategories() throws  Exception {
        when(this.recipeCategoryRepository.findAll()).thenReturn(recipeCategories);
        List<RecipeCategory> serviceList=recipeCategoryService.getAllCategories();
        assertEquals(2, serviceList.size());
        assertEquals(serviceList, recipeCategories);
        verify(recipeCategoryRepository,times(1)).findAll();
    }

    @Test
    public void getByValidId() throws  Exception {
        when(this.recipeCategoryRepository.getById(anyLong())).thenReturn(recipeCategory1);
        when(this.recipeCategoryRepository.existsById(anyLong())).thenReturn(recipeCategories.contains(recipeCategory1));
        RecipeCategory recipeCategory=recipeCategoryService.getById(1L);
        assertNotNull(recipeCategory);
        assertEquals(recipeCategory, recipeCategory1);
        verify(recipeCategoryRepository,times(1)).getById(anyLong());
        verify(recipeCategoryRepository,times(1)).existsById(anyLong());
    }

    @Test
    void saveRecipeCategory() {

        when(recipeCategoryRepository.save(any())).thenReturn(recipeCategory1);
        RecipeCategory saved = recipeCategoryService.save(recipeCategory1);
        assertEquals(recipeCategory1, saved);
        verify(recipeCategoryRepository,times(1)).save(any());
    }

    @Test
    public void deleteById(){
        doNothing().when(recipeCategoryRepository).delete(any());
        recipeCategoryService.delete(recipeCategory1);
        verify(recipeCategoryRepository,times(1)).delete(recipeCategory1);
    }

}