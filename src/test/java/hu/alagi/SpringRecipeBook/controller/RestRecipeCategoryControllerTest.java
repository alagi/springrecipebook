package hu.alagi.SpringRecipeBook.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.alagi.SpringRecipeBook.SpringRecipeBookApplication;
import hu.alagi.SpringRecipeBook.controllers.rest.RecordNotFoundException;
import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.service.RecipeCategoryService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Log4j2
@SpringBootTest(classes = SpringRecipeBookApplication.class)
@AutoConfigureMockMvc

public class RestRecipeCategoryControllerTest {

    private static List<RecipeCategory> categoryList;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RecipeCategoryService recipeCategoryServiceMocked;

    private ObjectMapper mapper;

    @BeforeEach
    public void init() {
        mapper = new ObjectMapper();
    }


    @BeforeAll
    public static void setupData() {
        log.info("---------- Start TEST ----------");
        RecipeCategory recipeCategory1 = new RecipeCategory("Levesek", "levesek kategóriája", null);
        recipeCategory1.setId(1L);
        RecipeCategory recipeCategory2 = new RecipeCategory("Főzelékek","finom főzelékek", null);
        recipeCategory2.setId(2L);
        RecipeCategory recipeCategory3 = new RecipeCategory("Mártások", "márt", null);
        recipeCategory3.setId(3L);
        categoryList = Arrays.asList(recipeCategory1, recipeCategory2, recipeCategory3);
    }

    @Test
    public void getAllRecipeCategories() throws Exception {
        String responseBody = mapper.writeValueAsString(categoryList);

        when(recipeCategoryServiceMocked.getAllCategories()).thenReturn(categoryList);

        this.mvc.perform(get("/api/v1/recipe-categories")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(responseBody));
    }

    @Test
    public void getEmptyRecipeCategories() throws Exception {

        when(recipeCategoryServiceMocked.getAllCategories()).thenReturn(Collections.emptyList());

        this.mvc.perform(get("/api/v1/recipe-categories")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getRecipeCategoryById() throws Exception {

        RecipeCategory recipeCategory = categoryList.get(0);

        when(recipeCategoryServiceMocked.getById(eq(recipeCategory.getId()))).thenReturn(recipeCategory);

        mvc.perform(get("/api/v1/recipe-categories/{recipeCategoryId}", recipeCategory.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("Levesek"))
                .andExpect(jsonPath("$.description").value("levesek kategóriája"));

        verify(recipeCategoryServiceMocked,times(1)).getById(eq(recipeCategory.getId()));
    }

    @Test
    public void getRecipeCategoryByWrongId() throws Exception {
        String errorMessage = "Record not found";
        when(recipeCategoryServiceMocked.getById(anyLong())).thenThrow(new RecordNotFoundException("Record not found"));

        mvc.perform(get("/api/v1/recipe-categories/{recipeCategoryId}", 12)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.name").doesNotExist())
                .andExpect(jsonPath("$.description").doesNotExist())
                .andExpect(jsonPath("$.errorMessage").value(errorMessage));

        verify(recipeCategoryServiceMocked,times(1)).getById(anyLong());
    }

    @Test
    public void createCategoryWithValidFields() throws Exception {

        RecipeCategory recipeCategory = categoryList.get(2);
        String cusJson = mapper.writeValueAsString(recipeCategory);

        recipeCategory.setId((long) (categoryList.indexOf(recipeCategory) + 1));

        when(recipeCategoryServiceMocked.save(eq(recipeCategory))).thenReturn(recipeCategory);

        mvc.perform(post("/api/v1/recipe-categories")
                        .content(cusJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(recipeCategory.getName()))
                .andExpect(jsonPath("$.description").value(recipeCategory.getDescription()))
                .andExpect(jsonPath("$.id").value(recipeCategory.getId()));

        verify(recipeCategoryServiceMocked,times(1)).save(eq(recipeCategory));
    }

    @Test
    public void patchCategoryWithValidFields() throws Exception {

        RecipeCategory recipeCategory = categoryList.get(2);
        log.info(recipeCategory);
        RecipeCategory patchCategory = new RecipeCategory("Saláták", null, null);
        String cusJson = mapper.writeValueAsString(patchCategory);
       // String cusJson = "{\"name\"=\"Saláták\"}";
        recipeCategory.setName("Saláták");
        when(recipeCategoryServiceMocked.patch(eq(patchCategory), eq(recipeCategory.getId()))).thenReturn(recipeCategory);

        mvc.perform(patch("/api/v1/recipe-categories/{recipeCategoryId}", recipeCategory.getId())
                        .content(cusJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(recipeCategory.getName()))
                .andExpect(jsonPath("$.description").value(recipeCategory.getDescription()));

        verify(recipeCategoryServiceMocked,times(1)).patch(eq(patchCategory),eq(recipeCategory.getId()));
    }

    @Test
    public void deleteCategoryByValidId() throws Exception {
        RecipeCategory recipeCategory = categoryList.get(0);

        when(recipeCategoryServiceMocked.getById(eq(recipeCategory.getId()))).thenReturn(recipeCategory);
        willDoNothing().given(recipeCategoryServiceMocked).delete(recipeCategory.getId());
        mvc.perform(delete("/api/v1/recipe-categories/{recipeCategoryId}",recipeCategory.getId()))
                .andExpect(status().isNoContent())
                        .andDo(print());

        verify(recipeCategoryServiceMocked,times(1)).delete(eq(recipeCategory.getId()));
    }

    @Test
    public void deleteCategoryByInValidId() throws Exception {
        Long recipeCategoryId = 12L;
        when(recipeCategoryServiceMocked.getById(eq(recipeCategoryId))).thenReturn(null);
        willDoNothing().given(recipeCategoryServiceMocked).delete(recipeCategoryId);
        mvc.perform(delete("/api/v1/recipe-categories/{recipeCategoryId}",recipeCategoryId))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(recipeCategoryServiceMocked,times(0)).delete(eq(recipeCategoryId));
    }

    @Test
    public void createCategoryWithInvalidName() throws Exception {

        RecipeCategory recipeCategory = new RecipeCategory();
        recipeCategory.setName("e"); // size min = 2!
        String responseBody = mapper.writeValueAsString(recipeCategory);

        when(recipeCategoryServiceMocked.save(eq(recipeCategory))).thenReturn(null);
        mvc.perform(post("/api/v1/recipe-categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(responseBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorMessage").value("Validation failed. 1 error(s)"));
        verify(recipeCategoryServiceMocked,times(0)).save(eq(recipeCategory));
    }
}
