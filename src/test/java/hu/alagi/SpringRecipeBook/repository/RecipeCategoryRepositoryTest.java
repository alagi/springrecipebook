package hu.alagi.SpringRecipeBook.repository;

import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Log4j2
@DataJpaTest
class RecipeCategoryRepositoryTest {


    private final RecipeCategoryRepository recipeCategoryRepository;

    public RecipeCategoryRepositoryTest(@Autowired RecipeCategoryRepository recipeCategoryRepository) {
        this.recipeCategoryRepository = recipeCategoryRepository;
    }

    private RecipeCategory recipeCategory;

    @BeforeEach
    public void setup() {
        this.recipeCategory=RecipeCategory.builder()
                .name("Mártások")
                .description("Szószók, mártások")
                .recipes(null)
                .build();
    }

    @AfterEach
    public void tearDown() {
        this.recipeCategoryRepository.deleteAll();
    }

    @Test
    void testInjectedRepository() {
        assertThat(recipeCategoryRepository).isNotNull();
    }


    @Test
    void saveTest() {
        log.traceEntry();
        log.info("TEST: From saveTest: "+this.recipeCategory);
        long currentCount = recipeCategoryRepository.count();
        RecipeCategory saved = recipeCategoryRepository.save(this.recipeCategory);
        assertThat(saved).isNotNull();
        assertThat(saved.getId()).isGreaterThan(0);
        RecipeCategory fetchedRecipeCategory=this.recipeCategoryRepository.findRecipeCategoryByName("Mártások");
        assertThat(fetchedRecipeCategory).isNotNull();
        assertThat(saved).hasFieldOrPropertyWithValue("name", "Mártások");
        assertEquals(currentCount+1, recipeCategoryRepository.count());
    }

    @Test
    public void testRecipeCategoryCounter() {
        assertEquals(6, recipeCategoryRepository.count());
    }

    @Test
    void findByName() {
        RecipeCategory fetchedRecipeCategory = this.recipeCategoryRepository.findRecipeCategoryByName("Mártások");
        Assert.isNonEmpty(fetchedRecipeCategory);
    }

    @Test
    void getById() {
        //recipeCategoryRepository.save(this.recipeCategory);
        RecipeCategory saved = recipeCategoryRepository.save(this.recipeCategory);
        log.info("TEST: id "+saved.getId());
        RecipeCategory fetchedRecipeCategory=this.recipeCategoryRepository.getById(saved.getId());
        assertEquals(fetchedRecipeCategory, saved);
    }
}