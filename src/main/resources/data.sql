INSERT INTO users (username, password, first_name, last_name, email, is_active, is_mfa_enabled, secret)
    VALUES ('mgodisai', '$2a$10$957mcsZgf5hIDeaDoXfBCO7qeaAJbFSDgXfz3nmPxDq8QU2oLZ4dC', 'Tamas', 'Varga', 'mgodisai@gmail.com', true, true, 'ERN7C3DG3GWBKDF6JXRCQIAF4M24GMQ7NZXL5JF4XPQU45N3R642VTCKHTIRU72W'),
           ('testadmin', '$2a$10$957mcsZgf5hIDeaDoXfBCO7qeaAJbFSDgXfz3nmPxDq8QU2oLZ4dC', 'testadmin', '', 'testadmin@test.hu', true, false, ''),
           ('testuser', '$2a$10$957mcsZgf5hIDeaDoXfBCO7qeaAJbFSDgXfz3nmPxDq8QU2oLZ4dC', 'testuser', '', 'testuser@test.hu', true, false, '');


INSERT INTO user_roles (r_username, roles)
    VALUES ('mgodisai', 'ADMIN'),
           ('mgodisai', 'USER'),
           ('testadmin', 'ADMIN'),
           ('testadmin', 'USER'),
            ('testuser', 'USER');

INSERT INTO ingredient (name, description, version, created, updated)
    VALUES  ('Sárgarépa', 'hosszú, magyar',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Liszt', 'BL55',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Víz', 'szénsavas',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Tojás', 'L-es méret',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Cukor', 'L-es méret',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Só', 'asztali',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO recipe_category (name, description, version, created, updated)
    VALUES  ('Pizzák', 'alaptészták, feltétötletekkel',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Levesek', '',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Főzelékek', '',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Egytálételek', '',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Saláták', '',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Zöldséges ételek', '',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO unit_of_measure (abbreviation, long_name, version, created, updated)
    VALUES  ('kg', 'kilogramm',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('dkg', 'dekagramm',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('g', 'gramm',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('l', 'liter',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('dl', 'deciliter',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('cl', 'centiliter',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('ml', 'milliliter',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('tk', 'teáskanál',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('ek', 'evőkanál',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('csipet', 'csipet',0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO recipe (name, description, difficulty, servings, ptime, ctime, version, created, updated)
    VALUES  ('Répakrémleves', 'Finom leves', 'KÖNNYŰ', 4, 30, 120,0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Zöldbabfőzelék', 'Finom főzelék', 'KÖNNYŰ', 4, 30, 60, 0,CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP()),
            ('Pizza', 'Alaptészta', 'KÖZEPES', 4, 30, 0, 0,CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

INSERT INTO recipe_recipe_category(recipe_id, category_id)
    VALUES  (1, 2),
            (1, 6),
            (2, 3),
            (2, 6),
            (3, 1);

INSERT INTO recipe_ingredient(recipe_id, uom_id, ingredient_id, amount)
    VALUES  (3,2,2,600),
            (3,3,6,10),
            (3,5,3,3);
