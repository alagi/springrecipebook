$(function () {
    $("#categorySelect").change(function () {
        var selectedText = $(this).find("option:selected").text();
        var selectedValue = $(this).val();
        window.location.replace('/recipe/category/' + selectedValue);
    });
});