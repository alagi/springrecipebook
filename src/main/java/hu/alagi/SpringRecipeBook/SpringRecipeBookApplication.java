package hu.alagi.SpringRecipeBook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"hu.alagi.SpringRecipeBook.entity"})
@EnableJpaRepositories(value = {"hu.alagi.SpringRecipeBook.repository"})
@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class SpringRecipeBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRecipeBookApplication.class, args);
	}
}