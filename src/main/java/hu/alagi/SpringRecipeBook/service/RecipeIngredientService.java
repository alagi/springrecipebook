package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import hu.alagi.SpringRecipeBook.entity.RecipeIngredient;
import hu.alagi.SpringRecipeBook.entity.RecipeIngredientKey;
import hu.alagi.SpringRecipeBook.repository.RecipeIngredientRepository;
import hu.alagi.SpringRecipeBook.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class RecipeIngredientService {
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final RecipeRepository recipeRepository;

    public RecipeIngredientService(
            @Autowired RecipeIngredientRepository recipeIngredientRepository,
            @Autowired RecipeRepository recipeRepository) {
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.recipeRepository = recipeRepository;
    }

    public List<RecipeIngredient> getRecipeIngredientsByRecipe(Recipe recipe) {
        if (recipeRepository.getById(recipe.getId()) != null) {
            return recipeIngredientRepository.getRecipeIngredientsByRecipe(recipe);
        } else {
            return Collections.emptyList();
        }
    }

    public RecipeIngredient save(RecipeIngredient recipeIngredient) {
//        if (recipeIngredient.getId()!=null) {
//            RecipeIngredient ring = this.recipeIngredientRepository.getById(recipeIngredient.getId());
//            if (ring != null) {
//                return this.recipeIngredientRepository.save(recipeIngredient);
//            }
//            ring.setIngredient(recipeIngredient.getIngredient());
//            ring.setRecipe(recipeIngredient.getRecipe());
//            ring.setUnitOfMeasure(recipeIngredient.getUnitOfMeasure());
//            ring.setAmount(recipeIngredient.getAmount());
//            return this.recipeIngredientRepository.save(ring);
//        }
        return this.recipeIngredientRepository.save(recipeIngredient);
    }

    public RecipeIngredient getByCompId(RecipeIngredientKey ring_id) {
        return this.recipeIngredientRepository.getRecipeIngredientByCompId(ring_id);
    }

    public void delete(RecipeIngredient ring) {
        this.recipeIngredientRepository.delete(ring);
    }
}
