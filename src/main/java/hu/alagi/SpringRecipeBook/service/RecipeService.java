package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RecipeService {
    public List<Recipe> getAllRecipes();
    public List<Recipe> getRecipesByRecipeCategoryId(Long catID);
    public List<Recipe> getRecipesByKeyword(String keyword);
    public Integer countRecipesByKeyword(String keyword);
    public Recipe getRecipeById(Long id);
    public void deleteRecipeById(Long id);
    public Long countAllRecipes();
    public long countRecipesByCategoryId(Long id);
    public Recipe save(Recipe recipe);
}
