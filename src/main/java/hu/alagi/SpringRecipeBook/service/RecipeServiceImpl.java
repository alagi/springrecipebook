package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.repository.RecipeCategoryRepository;
import hu.alagi.SpringRecipeBook.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService {

    // Fields
    private final RecipeRepository recipeRepository;
    private final RecipeCategoryRepository recipeCategoryRepository;

    // Constructor
    public RecipeServiceImpl(@Autowired RecipeRepository recipeRepository, @Autowired RecipeCategoryRepository recipeCategoryRepository) {
        this.recipeRepository = recipeRepository;
        this.recipeCategoryRepository = recipeCategoryRepository;
    }

    // Methods
    public List<Recipe> getAllRecipes() {
        return this.recipeRepository.findAll();
    }

    public List<Recipe> getRecipesByRecipeCategoryId(Long catID) {
        return this.recipeRepository.getRecipesByRecipeCategoriesId(catID);
    }

    public List<Recipe> getRecipesByKeyword(String keyword) {
        return this.recipeRepository.getRecipesByNameContainsIgnoreCaseOrDescriptionContainsIgnoreCase(keyword, keyword);
    }

    public Integer countRecipesByKeyword(String keyword) {
        return this.recipeRepository.countRecipesByNameContainsIgnoreCaseOrDescriptionContainsIgnoreCase(keyword, keyword);
    }

    public Recipe getRecipeById(Long id) {
        Recipe rcp;
        if (recipeRepository.existsById(id)) {
            rcp = recipeRepository.getById(id);
        } else {
            rcp = null;
        }
        return rcp;
    }

    public void deleteRecipeById(Long id) {
        this.recipeRepository.deleteById(id);
    }

    public Long countAllRecipes() {
        return this.recipeRepository.count();
    }

    public long countRecipesByCategoryId(Long id) {
        return this.recipeRepository.countRecipeByRecipeCategoriesId(id);
    }

    public Recipe save(Recipe recipe) {
        if (recipe.getId()!=null) {
            Recipe rcp = this.recipeRepository.getById(recipe.getId());
            if (rcp==null)
                return this.recipeRepository.save(recipe);
            rcp.setName(recipe.getName());
            rcp.setDescription(recipe.getDescription());
            rcp.setRecipeCategories(recipe.getRecipeCategories());
            rcp.setDifficulty(recipe.getDifficulty());
            rcp.setCookingTime(recipe.getCookingTime());
            rcp.setPreparationTime(recipe.getPreparationTime());
            rcp.setServings(recipe.getServings());
            return this.recipeRepository.save(rcp);
        }
        List<RecipeCategory> updatedCategoryList = new ArrayList<>();
        for (RecipeCategory rcat : recipe.getRecipeCategories()) {
                if (recipeCategoryRepository.findRecipeCategoryByName(rcat.getName())==null) {
                    rcat = recipeCategoryRepository.save(rcat);
                }

                else {
                    rcat = recipeCategoryRepository.findRecipeCategoryByName(rcat.getName());
                    recipeCategoryRepository.save(rcat);
                }
                updatedCategoryList.add(rcat);

        }
        recipe.setRecipeCategories(updatedCategoryList);
        return this.recipeRepository.save(recipe);
    }


}
