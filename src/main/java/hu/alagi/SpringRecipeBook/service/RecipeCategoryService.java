package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.repository.RecipeCategoryRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Log4j2
@Service
@Transactional
public class RecipeCategoryService {

    // fields
    private final RecipeCategoryRepository categoryRepository;

    // constructor
    public RecipeCategoryService(@Autowired RecipeCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<RecipeCategory> getAllCategories() {
        return this.categoryRepository.findAll();
    }

    public RecipeCategory getById(Long id) {
        RecipeCategory rcat;
        if (categoryRepository.existsById(id)) {
            rcat = categoryRepository.getById(id);
        } else {
            rcat = null;
        }
        return rcat;
    }

    public void delete(Long id) {
        this.categoryRepository.deleteById(id);
    }

    public void delete(RecipeCategory recipeCategory) {
        this.categoryRepository.delete(recipeCategory);
    }

    public long countAll() {
        return this.categoryRepository.count();
    }

    public RecipeCategory updateOrCreate(RecipeCategory updateCategory, Long id) {
        updateCategory.setId(id);
        return this.save(updateCategory);
    }

    public RecipeCategory patch(RecipeCategory updateCategory, Long id) {
        RecipeCategory modifiedCategory;
        if (categoryRepository.existsById(id)) {
            modifiedCategory = categoryRepository.getById(id);
            if (updateCategory.getName()!=null)
                modifiedCategory.setName(updateCategory.getName());
            if (updateCategory.getDescription()!=null)
                modifiedCategory.setDescription(updateCategory.getDescription());
            return this.save(modifiedCategory);
        } else {
            return null;
        }
    }

    public RecipeCategory save(RecipeCategory category) {
        if (category.getId()!=null) {
            RecipeCategory cat;
            if (categoryRepository.existsById(category.getId())) {
                cat = this.categoryRepository.getById(category.getId());
            } else {
                cat = new RecipeCategory();
            }
            cat.setName(category.getName());
            cat.setDescription(category.getDescription());
            return this.categoryRepository.save(cat);
        }
        return this.categoryRepository.save(category);
    }
}
