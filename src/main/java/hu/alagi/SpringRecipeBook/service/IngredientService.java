package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.Ingredient;
import hu.alagi.SpringRecipeBook.repository.IngredientRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Log4j2
@Service
@Transactional
public class IngredientService {

    // fields
    private final IngredientRepository ingredientRepository;

    // constructor
    public IngredientService(@Autowired IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public List<Ingredient> getAllIngredients() {
        return this.ingredientRepository.findAll();
    }

    public Ingredient getById(Long id) {
        Ingredient ing;
        if (ingredientRepository.existsById(id)) {
            ing = ingredientRepository.getById(id);
        } else {
            ing = null;
        }
        return ing;
    }

    @Transactional
    public void delete(Long id) {
        this.ingredientRepository.deleteById(id);
    }

    public long countAll() {
        return this.ingredientRepository.count();
    }

    public Ingredient save(Ingredient ingredient) {
        if (ingredient.getId()!=null) {
            Ingredient ing = this.ingredientRepository.getById(ingredient.getId());
            if (ing==null)
                return this.ingredientRepository.save(ingredient);
            ing.setName(ingredient.getName());
            ing.setDescription(ingredient.getDescription());
            return this.ingredientRepository.save(ing);
        }
        return this.ingredientRepository.save(ingredient);
    }
}
