package hu.alagi.SpringRecipeBook.service;

import hu.alagi.SpringRecipeBook.entity.UnitOfMeasure;
import hu.alagi.SpringRecipeBook.repository.UnitOfMeasureRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Log4j2
@Service
@Transactional
public class UnitOfMeasureService {

    // fields
    private final UnitOfMeasureRepository unitOfMeasureRepository;

    // constructor
    public UnitOfMeasureService(@Autowired UnitOfMeasureRepository unitOfMeasureRepository) {
        this.unitOfMeasureRepository = unitOfMeasureRepository;
    }

    // methods
    public List<UnitOfMeasure> getAllCategories() {
        return this.unitOfMeasureRepository.findAll();
    }

    public UnitOfMeasure getById(Long id) {
        UnitOfMeasure uom;
        if (unitOfMeasureRepository.existsById(id)) {
            uom = unitOfMeasureRepository.getById(id);
        } else {
            uom = null;
        }
        return uom;
    }

    @Transactional
    public void delete(Long id) {
        this.unitOfMeasureRepository.deleteById(id);
    }

    public long countAll() {
        return this.unitOfMeasureRepository.count();
    }

    public UnitOfMeasure save(UnitOfMeasure unit) {
        if (unit.getId()!=null) {
            UnitOfMeasure u = this.unitOfMeasureRepository.getById(unit.getId());
            if (u==null)
                return this.unitOfMeasureRepository.save(unit);
            u.setAbbreviation(unit.getAbbreviation());
            u.setLongName(unit.getLongName());
            return this.unitOfMeasureRepository.save(u);
        }
        return this.unitOfMeasureRepository.save(unit);
    }
}
