package hu.alagi.SpringRecipeBook.repository;

import hu.alagi.SpringRecipeBook.entity.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitOfMeasureRepository extends JpaRepository<UnitOfMeasure, Long> {
    UnitOfMeasure findByAbbreviation(String abbreviation);
}
