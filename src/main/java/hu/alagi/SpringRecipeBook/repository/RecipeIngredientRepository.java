package hu.alagi.SpringRecipeBook.repository;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import hu.alagi.SpringRecipeBook.entity.RecipeIngredient;
import hu.alagi.SpringRecipeBook.entity.RecipeIngredientKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeIngredientRepository extends JpaRepository<RecipeIngredient, RecipeIngredientKey> {
    List<RecipeIngredient> getRecipeIngredientsByRecipe(Recipe recipe);

    RecipeIngredient getRecipeIngredientByCompId(RecipeIngredientKey k);
}
