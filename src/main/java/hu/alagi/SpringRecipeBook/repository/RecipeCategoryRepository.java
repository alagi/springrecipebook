package hu.alagi.SpringRecipeBook.repository;

import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RecipeCategoryRepository extends JpaRepository<RecipeCategory, Long> {

    RecipeCategory findRecipeCategoryByName(String Name);


}
