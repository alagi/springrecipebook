package hu.alagi.SpringRecipeBook.repository;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import org.hibernate.annotations.NamedQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    Recipe findByName(String name);

    Recipe getById(Long id);

    List<Recipe> getRecipesByRecipeCategoriesId(Long categoryID);

    List<Recipe> getRecipesByNameContainsIgnoreCaseOrDescriptionContainsIgnoreCase(String keyword1, String keyword2);

    Integer countRecipesByNameContainsIgnoreCaseOrDescriptionContainsIgnoreCase(String keyword1, String keyword2);

    Integer countRecipeByRecipeCategoriesId(Long categoryId);
}
