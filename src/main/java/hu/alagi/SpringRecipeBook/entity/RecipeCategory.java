package hu.alagi.SpringRecipeBook.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name=RecipeCategory.TBL_NAME)
public class RecipeCategory extends AbstractEntity<Long> implements Serializable {
    private static final String FLD_NAME = "name";
    public static final String FLD_DESC = "description";
    public static final String TBL_NAME = "recipe_category";

    @Column(name = FLD_NAME, nullable = false, unique = true)
    @NotNull(message = "{validation.name}")
    @Size(min=2, max = 100, message="{validation.size}")
    private String name;

    @Column(name = FLD_DESC)
    private String description;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "recipeCategories")
    private List<Recipe> recipes;
}
