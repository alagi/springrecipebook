package hu.alagi.SpringRecipeBook.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name=Ingredient.TBL_NAME)
public class Ingredient extends AbstractEntity<Long> {

    public static final String TBL_NAME = "ingredient";
    public static final String FLD_NAME = "name";
    public static final String FLD_DESC = "description";

    @NotNull(message = "{validation.notnull}")
    @Size(min=2, max = 100, message="{validation.size}")
    @Column(name = FLD_NAME, nullable = false)
    private String name;

    @Column(name = FLD_DESC)
    private String description;

    @JsonBackReference
    @OneToMany(mappedBy = "ingredient")
    private List<RecipeIngredient> recipeIngredients = new ArrayList<>();
}
