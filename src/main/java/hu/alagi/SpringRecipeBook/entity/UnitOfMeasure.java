package hu.alagi.SpringRecipeBook.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name=UnitOfMeasure.TBL_NAME)
public class UnitOfMeasure extends AbstractEntity<Long> {
    public static final String TBL_NAME = "unit_of_measure";
    public static final String FLD_ABBR = "abbreviation";
    public static final String FLD_LONG_NAME = "long_name";

    @NotNull(message="{validation.notnull}")
    @Size(min=1, max=10, message="{validation.size}")
    @Column(name=FLD_ABBR)
    private String abbreviation;

    @Column(name=FLD_LONG_NAME)
    private String longName;

    @JsonBackReference
    @OneToMany(mappedBy = "unitOfMeasure")
    private List<RecipeIngredient> recipeIngredients = new ArrayList<>();
}
