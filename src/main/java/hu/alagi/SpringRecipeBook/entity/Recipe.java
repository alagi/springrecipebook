package hu.alagi.SpringRecipeBook.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = false)
@SuperBuilder
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = Recipe.TBL_NAME)
public class Recipe extends AbstractEntity<Long> implements Serializable {
    public static final String TBL_NAME = "recipe";
    public static final String FLD_NAME = "name";
    public static final String FLD_DESC = "description";
    public static final String FLD_SERVINGS = "servings";
    public static final String FLD_DIFFICULTY = "difficulty";
    public static final String FLD_CTIME = "ctime";
    public static final String FLD_PTIME = "ptime";

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="recipe_recipe_category",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<RecipeCategory> recipeCategories;

    @Column(name = FLD_NAME, nullable = false)
    @NotNull(message = "{validation.notnull}")
    @Size(min=2, max = 100, message="{validation.recipe.name.size}")
    private String name;

    @Column(name = FLD_DESC, nullable = false)
    @NotNull(message = "{validation.notnull}")
    private String description;

    @Column(name = FLD_SERVINGS, nullable = false)
    @NotNull(message = "{validation.recipe.servings.notnull}")
    @Min(value = 1, message ="{validation.recipe.servings.min}")
    @Max(value=100, message ="{validation.recipe.servings.max}")
    private Integer servings;

    @JsonManagedReference
    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private List<RecipeIngredient> recipeIngredients = new ArrayList<>();

    @NotNull(message="{validation.notnull}")
    @Column(name = FLD_DIFFICULTY, nullable = false)
    @Enumerated(value=EnumType.STRING)
    private Difficulty difficulty;

    @Positive(message="{validation.positive}")
    @Column(name = FLD_CTIME, nullable = true)
    private Integer cookingTime;

    @Positive(message="{validation.positive}")
    @Column(name = FLD_PTIME, nullable = true)
    private Integer preparationTime;
}
