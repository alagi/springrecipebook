package hu.alagi.SpringRecipeBook.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class AbstractEntity<ID extends Serializable> implements Serializable {
    private static final String FLD_ID="id";
    private static final String FLD_VERSION ="version";
    private static final String FLD_CREATED ="created";
    private static final String FLD_UPDATED ="updated";

    @Id
    @Column(name = FLD_ID, unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID id;

    @JsonIgnore
    @Version
    @Column(name = FLD_VERSION)
    private Integer version;

    @JsonIgnore
    @CreationTimestamp
    @Column(name = FLD_CREATED)
    private LocalDateTime created;

    @JsonIgnore
    @UpdateTimestamp
    @Column(name = FLD_UPDATED)
    private LocalDateTime updated;
}
