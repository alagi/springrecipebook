package hu.alagi.SpringRecipeBook.entity.security;

import org.springframework.security.core.GrantedAuthority;

public enum UserRole implements GrantedAuthority {
    ADMIN("ADMIN"), USER("USER");

    public final String name;

    UserRole(String name) {
        this.name = name;
    }
    @Override
    public String getAuthority() {
        return "ROLE_"+name();
    }
}
