package hu.alagi.SpringRecipeBook.entity.security;

import hu.alagi.SpringRecipeBook.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = false)
@Entity
@NoArgsConstructor
@SuperBuilder
@Table(name=UserEntity.TBL_NAME)
public class UserEntity extends AbstractEntity<Long> {
    public static final String TBL_NAME="users";
    public static final String TBL_ROLES="user_roles";
    public static final String FLD_USERNAME ="username";
    public static final String FLD_PASSWORD="password";
    public static final String FLD_FIRST_NAME="first_name";
    public static final String FLD_LAST_NAME="last_name";
    public static final String FLD_EMAIL="email";
    public static final String FLD_USER_ROLES="roles";
    public static final String FLD_ACTIVE="is_active";
    public static final String FLD_MFA_ENABLED="is_mfa_enabled";
    public static final String FLD_SECRET="secret";


    @NotNull
    @Column(name= FLD_USERNAME, nullable = false, unique = true, updatable = false)
    @Size(min=4, max=25, message="{validation.username.size}")
    private String username;

    @NotNull
    @Size(min=8, message = "{validation.password.size")
    @Column(name=FLD_PASSWORD, nullable = false)
    private String password;

    @Column(name = FLD_FIRST_NAME)
    private String firstName;

    @Column(name=FLD_LAST_NAME)
    private String lastName;

    @Column(name=FLD_EMAIL)
    @NotEmpty(message="{validation.email.notempty}")
    private String email;

    @ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
    @JoinTable(name=UserEntity.TBL_ROLES, joinColumns=@JoinColumn(name= "r_username", referencedColumnName = FLD_USERNAME))
    @Column(name = FLD_USER_ROLES, nullable = false)
    @Enumerated(EnumType.STRING)
    private Set<UserRole> roles;

    @NotNull
    @Column(name=FLD_ACTIVE)
    private Boolean isActive;

    @NotNull
    @Column(name = FLD_MFA_ENABLED)
    private Boolean isMfaEnabled;

    @Column(name = FLD_SECRET)
    private String secret;
}
