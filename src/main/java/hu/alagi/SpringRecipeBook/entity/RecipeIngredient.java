package hu.alagi.SpringRecipeBook.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name=RecipeIngredient.TBL_NAME)
public class RecipeIngredient {
    public static final String TBL_NAME = "recipe_ingredient";
    public static final String FLD_AMOUNT = "amount";

    @JsonIgnore
    @EmbeddedId
    RecipeIngredientKey compId;

    @JsonBackReference
    @ManyToOne
    @NotNull(message="{validation.notnull")
    @MapsId("recipeId")
    @JoinColumn(name = RecipeIngredientKey.FLD_RECIPE_ID)
    private Recipe recipe;

    @ManyToOne
    @NotNull(message="{validation.notnull}")
    @MapsId("ingredientId")
    @JoinColumn(name=RecipeIngredientKey.FLD_INGREDIENT_ID)
    private Ingredient ingredient;

    @ManyToOne
    @NotNull(message="{validation.notnull}")
    @MapsId("uomId")
    @JoinColumn(name=RecipeIngredientKey.FLD_UOM_ID)
    private UnitOfMeasure unitOfMeasure;

    // extra column
    @NotNull(message="{validation.notnull}")
    @Min(value=0, message="{validation.positive}")
    @Column(name=FLD_AMOUNT)
    private Double amount;
}
