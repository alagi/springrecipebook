package hu.alagi.SpringRecipeBook.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

// https://www.baeldung.com/jpa-many-to-many

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class RecipeIngredientKey implements Serializable {
    public static final String FLD_RECIPE_ID = "recipe_id";
    public static final String FLD_INGREDIENT_ID = "ingredient_id";
    public static final String FLD_UOM_ID = "uom_id";

    @Column(name = FLD_RECIPE_ID)
    Long recipeId;
    @Column(name = FLD_INGREDIENT_ID)
    Long ingredientId;
    @Column(name = FLD_UOM_ID)
    Long uomId;
}
