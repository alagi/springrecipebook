package hu.alagi.SpringRecipeBook.entity;

public enum Difficulty {
    KÖNNYŰ, KÖZEPES, NEHÉZ
}
