package hu.alagi.SpringRecipeBook.security;

import hu.alagi.SpringRecipeBook.entity.security.UserEntity;
import hu.alagi.SpringRecipeBook.entity.security.UserRole;
import hu.alagi.SpringRecipeBook.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class UserEntityDetailsService implements UserDetailsService {
    private final UserEntityDetailsMapper userEntityDetailsMapper;
    private final UserEntityRepository userEntityRepository;

    public UserEntityDetailsService(
            @Autowired UserEntityDetailsMapper userEntityDetailsMapper,
            @Autowired UserEntityRepository userEntityRepository) {
        this.userEntityDetailsMapper = userEntityDetailsMapper;
        this.userEntityRepository = userEntityRepository;
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity usr = this.userEntityRepository.findByUsername(username);
        if (usr == null)
            throw new UsernameNotFoundException(username);
        return this.userEntityDetailsMapper.toUserDetails(usr);
    }
}
