package hu.alagi.SpringRecipeBook.security;

import hu.alagi.SpringRecipeBook.entity.security.UserEntity;
import hu.alagi.SpringRecipeBook.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class UserEntityPasswordService implements UserDetailsPasswordService {

    private UserEntityDetailsMapper userEntityDetailsMapper;
    private UserEntityRepository userEntityRepository;

    public UserEntityPasswordService(
            @Autowired UserEntityDetailsMapper userEntityDetailsMapper,
            @Autowired UserEntityRepository userEntityRepository) {
        this.userEntityDetailsMapper = userEntityDetailsMapper;
        this.userEntityRepository = userEntityRepository;
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
       UserEntity usr=this.userEntityRepository.findByUsername(user.getUsername());

        return usr==null ? null :
                this.userEntityDetailsMapper.toUserDetails(usr);
    }
}
