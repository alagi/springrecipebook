package hu.alagi.SpringRecipeBook.security;

import hu.alagi.SpringRecipeBook.entity.security.UserEntity;
import hu.alagi.SpringRecipeBook.entity.security.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserEntityDetailsMapper {

    UserDetails toUserDetails(UserEntity userEntity) {
        List<GrantedAuthority> grantedAuthorities = userEntity.getRoles().stream()
                .map(authority-> new SimpleGrantedAuthority(authority.getAuthority())).collect(Collectors.toList());

        return User.withUsername(userEntity.getUsername())
                .password((userEntity.getPassword()))
                .roles(String.valueOf(grantedAuthorities))
                .build();
    }
}
