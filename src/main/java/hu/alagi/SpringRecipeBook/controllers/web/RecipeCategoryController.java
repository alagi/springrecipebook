package hu.alagi.SpringRecipeBook.controllers.web;

import hu.alagi.SpringRecipeBook.entity.Ingredient;
import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.service.RecipeCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/category")
public class RecipeCategoryController {

    private static final String FOLDER_CATEGORY = "category/";

    private final RecipeCategoryService categoryService;

    public RecipeCategoryController(@Autowired RecipeCategoryService categoryService) {
        this.categoryService= categoryService;
    }

    @GetMapping("/all")
    public String findAllCategory(Model model) {
        model.addAttribute("categories", categoryService.getAllCategories());
        model.addAttribute("numberOfCategories", categoryService.countAll());
        return FOLDER_CATEGORY+"categories";
    }

    @GetMapping("/edit/{id}")
    public String updateForm(
            @PathVariable("id") Long id,
            Model model,
            final RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "upd");
        RecipeCategory cat=categoryService.getById(id);
        if(cat!=null) {
            if (model.getAttribute("category") == null) {
                model.addAttribute("category", cat);
            }
            return FOLDER_CATEGORY+"category-form";
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Category not exists with id: "+id);
        }
        return "redirect:/category/all";
    }

    @PostMapping("/update/{id}")
    public String updateCategory(
            @PathVariable("id") Long id,
            @Valid @ModelAttribute("category") RecipeCategory category,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {

        category.setId(id);
        String view;

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Update failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", result);
            redirectAttributes.addFlashAttribute("category", category);
            view = "redirect:/category/edit/"+id;
        } else {
            categoryService.save(category);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Updated successfully: "+category.getName());
            view= "redirect:/category/all";
        }
        return view;
    }

    @GetMapping("/new")
    public String showNew(Model model) {
        model.addAttribute("action", "new");
        if (model.getAttribute("category") ==null) {
            model.addAttribute("category", new RecipeCategory());
        }
        return FOLDER_CATEGORY+"category-form";
    }

    @PostMapping("/add")
    public String addCategory(
            @Valid @ModelAttribute("category") final RecipeCategory category,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {
        String view;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "Adding new failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.category", result);
            redirectAttributes.addFlashAttribute("category", category);
            view = "redirect:/category/new/";
        } else {
            this.categoryService.save(category);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Added successfully: "+category.getName());
            view = "redirect:/category/all";
        }
        return view;
    }

    @GetMapping("/delete/{id}")
    public String deleteCategory(
            @PathVariable("id") Long id,
            RedirectAttributes redirectAttributes) {
        RecipeCategory category=this.categoryService.getById(id);

        if (category!=null) {
            try {
                this.categoryService.delete(id);
                redirectAttributes.addFlashAttribute("success",true);
                redirectAttributes.addFlashAttribute("message","Deleted successfully: "+category.getName());
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("success",false);
                redirectAttributes.addFlashAttribute("message","Can't delete due to DataIntegrity issue : "+category.getName());
            }
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Category not exists with id: "+id);
        }
        return "redirect:/category/all";
    }
}
