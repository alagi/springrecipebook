package hu.alagi.SpringRecipeBook.controllers.web;

import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.entity.UnitOfMeasure;
import hu.alagi.SpringRecipeBook.repository.IngredientRepository;
import hu.alagi.SpringRecipeBook.repository.RecipeCategoryRepository;
import hu.alagi.SpringRecipeBook.repository.RecipeRepository;
import hu.alagi.SpringRecipeBook.repository.UnitOfMeasureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping({"", "/", "/index"})
    public String getIndexPage() {
        return "index";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }
}
