package hu.alagi.SpringRecipeBook.controllers.web;

import hu.alagi.SpringRecipeBook.entity.Ingredient;
import hu.alagi.SpringRecipeBook.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/ingredient")
public class IngredientController {

    private static final String FOLDER_INGREDIENT = "ingredient/";

    private final IngredientService ingredientService;

    public IngredientController(@Autowired IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping("/all")
    public String findAllIngredient(Model model) {
        model.addAttribute("ingredients", ingredientService.getAllIngredients());
        model.addAttribute("numberOfIngredients", ingredientService.countAll());
        return FOLDER_INGREDIENT+"ingredients";
    }

    @GetMapping("/edit/{id}")
    public String updateForm(
            @PathVariable("id") Long id,
            Model model,
            final RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "upd");
        Ingredient ing=ingredientService.getById(id);
        if(ing!=null) {
            if (model.getAttribute("ingredient") == null) {
                model.addAttribute("ingredient", ing);
            }
            return FOLDER_INGREDIENT+"ingredient-form";
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Ingredient not exists with id: "+id);
        }
        return "redirect:/ingredient/all";
    }

    @PostMapping("/update/{id}")
    public String updateIngredient(
            @PathVariable("id") Long id,
            @Valid @ModelAttribute("ingredient") Ingredient ingredient,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {

        ingredient.setId(id);
        String view;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Update failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.ingredient", result);
            redirectAttributes.addFlashAttribute("ingredient", ingredient);
            view = "redirect:/ingredient/edit/"+id;
        } else {
            ingredientService.save(ingredient);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Updated successfully: "+ingredient.getName());
            view = "redirect:/ingredient/all";
        }
        return view;
    }

    @GetMapping("/new")
    public String showNew(Model model) {
        model.addAttribute("action", "new");
        if (model.getAttribute("ingredient") ==null) {
            model.addAttribute("ingredient", new Ingredient());
        }
        return FOLDER_INGREDIENT+"ingredient-form";
    }

    @PostMapping("/add")
    public String addIngredient(
            @Valid @ModelAttribute("ingredient") final Ingredient ingredient,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {
        String view;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "Adding new failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.ingredient", result);
            redirectAttributes.addFlashAttribute("ingredient", ingredient);
            view = "redirect:/ingredient/new/";
        } else {
            this.ingredientService.save(ingredient);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Added successfully: "+ingredient.getName());
            view = "redirect:/ingredient/all";
        }
        return view;
    }

    @GetMapping("/delete/{id}")
    public String deleteIngredient(
            @PathVariable("id") Long id,
            RedirectAttributes redirectAttributes) {
        Ingredient ingredient=this.ingredientService.getById(id);

        if (ingredient!=null) {
            try {
                this.ingredientService.delete(id);
                redirectAttributes.addFlashAttribute("success",true);
                redirectAttributes.addFlashAttribute("message","Deleted successfully: "+ingredient.getName());
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("success",false);
                redirectAttributes.addFlashAttribute("message","Can't delete due to DataIntegrity issue : "+ingredient.getName());
            }
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Ingredient not exists with id: "+id);
        }
        return "redirect:/ingredient/all";
    }
}
