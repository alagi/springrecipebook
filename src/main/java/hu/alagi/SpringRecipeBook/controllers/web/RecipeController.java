package hu.alagi.SpringRecipeBook.controllers.web;

import hu.alagi.SpringRecipeBook.entity.*;
import hu.alagi.SpringRecipeBook.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/recipe")
public class RecipeController {

    private static final String FOLDER_RECIPE = "recipe/";
    private final RecipeService recipeService;
    private final RecipeCategoryService recipeCategoryService;
    private final RecipeIngredientService recipeIngredientService;
    private final IngredientService ingredientService;
    private final UnitOfMeasureService unitOfMeasureService;

    public RecipeController(
            @Autowired RecipeServiceImpl recipeService,
            @Autowired RecipeCategoryService categoryService,
            @Autowired RecipeIngredientService recipeIngredientService,
            @Autowired IngredientService ingredientService,
            @Autowired UnitOfMeasureService unitOfMeasureService) {
        this.recipeCategoryService = categoryService;
        this.recipeService= recipeService;
        this.recipeIngredientService = recipeIngredientService;
        this.ingredientService = ingredientService;
        this.unitOfMeasureService = unitOfMeasureService;
    }

    @GetMapping("/all")
    public String findAllRecipeId(Model model) {
        model.addAttribute("selectedCategory", 0);
        model.addAttribute("categories", recipeCategoryService.getAllCategories());
        model.addAttribute("recipes", recipeService.getAllRecipes());
        model.addAttribute("numberOfRecipes", recipeService.countAllRecipes());
        return FOLDER_RECIPE +"recipes";
    }

    @GetMapping("/category/{categoryId}")
    public String findAllRecipeByCategory(@PathVariable("categoryId") Long categoryId, Model model) {
        if (categoryId==0) {
            return "redirect:/recipe/all";
        }
        model.addAttribute("selectedCategory", categoryId);
        model.addAttribute("categories", recipeCategoryService.getAllCategories());
        model.addAttribute("recipes", recipeService.getRecipesByRecipeCategoryId(categoryId));
        model.addAttribute("numberOfRecipes", recipeService.countRecipesByCategoryId(categoryId));

        return FOLDER_RECIPE +"recipes";
    }

    @GetMapping("/edit/{id}")
    public String updateForm(
            @PathVariable("id") Long id,
            Model model,
            final RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "upd");
        Recipe rcp=recipeService.getRecipeById(id);

        if(rcp!=null) {
            if (model.getAttribute("recipe") == null) {
                model.addAttribute("recipe", rcp);
            }

            model.addAttribute("recipeCategories", recipeCategoryService.getAllCategories());
            model.addAttribute("selectedCategories", rcp.getRecipeCategories());
            model.addAttribute("recipeIngredients", rcp.getRecipeIngredients());
            model.addAttribute("ingredients", ingredientService.getAllIngredients());
            model.addAttribute("unitsOfMeasure", unitOfMeasureService.getAllCategories());
            model.addAttribute("amount", null);
            if (model.getAttribute("showerror") == null) {
                model.addAttribute("showerror", false);
            }

            return FOLDER_RECIPE +"recipe-form";
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Recipe not exists with id: "+id);
        }
        return "redirect:/recipe/all";
    }

    @PostMapping("/update/{id}")
    public String updateRecipe(
            @PathVariable("id") Long id,
            @Valid @ModelAttribute("recipe") Recipe recipe,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {

        recipe.setId(id);

        String view;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Update failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.recipe", result);
            redirectAttributes.addFlashAttribute("recipe", recipe);
            view = "redirect:/recipe/edit/"+id;
        } else {
            recipeService.save(recipe);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Updated successfully: "+recipe.getName());
            view = "redirect:/recipe/all";
        }
        return view;
    }

    @GetMapping("/new")
    public String showNew(Model model, ModelMap modelMap, HttpServletRequest request) {
        model.addAttribute("action", "new");
        if (model.getAttribute("recipe") ==null) {
            model.addAttribute("recipe", new Recipe());
        }
        model.addAttribute("recipeCategories", recipeCategoryService.getAllCategories());
        return FOLDER_RECIPE +"recipe-form";
    }

    @PostMapping("/add")
    public String addRecipe(
            @Valid @ModelAttribute("recipe") Recipe recipe,
            final BindingResult result,
            final RedirectAttributes redirectAttributes) {
        final String view;

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "Adding new failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.recipe", result);
            redirectAttributes.addFlashAttribute("recipe", recipe);
            view = "redirect:/recipe/new/";
        } else {
            this.recipeService.save(recipe);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Added successfully: "+recipe.getName());
            view = "redirect:/recipe/edit/"+recipe.getId();
        }
        return view;
    }

    @PostMapping("/search")
    public String findRecipeByName(@ModelAttribute("keyword") String keyword, Model model) {
        model.addAttribute("recipes", recipeService.getRecipesByKeyword(keyword));
        model.addAttribute("numberOfRecipes", recipeService.countRecipesByKeyword(keyword));
        return FOLDER_RECIPE +"recipes";
    }

    @PostMapping(value = "/{id}/ingredient")
    public String addRecipeIngredient(
            @PathVariable("id") Long id,
            @Valid @ModelAttribute("recipeIngredient") RecipeIngredient recipeIngredient,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {

        if (result.hasFieldErrors("amount") || result.hasFieldErrors("unitOfMeasure") || result.hasFieldErrors("ingredient")) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "Adding new recipe ingredient failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.recipeIngredient", result);
            redirectAttributes.addFlashAttribute("recipeIngredient", recipeIngredient);
            redirectAttributes.addFlashAttribute("showerror", true);
            return "redirect:/recipe/edit/"+id;
        }
        String view;
        RecipeIngredientKey k = new RecipeIngredientKey(
                recipeIngredient.getRecipe().getId(),
                recipeIngredient.getIngredient().getId(),
                recipeIngredient.getUnitOfMeasure().getId());
        recipeIngredient.setCompId(k);
        RecipeIngredient r = recipeIngredientService.getByCompId(k);
        if (r != null) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "This ingredient with the given Unit is already add!");
            redirectAttributes.addFlashAttribute("showerror", false);
            view = "redirect:/recipe/edit/"+id;
        } else {
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Ingredient added successfully!");
            redirectAttributes.addFlashAttribute("showerror", false);
            recipeIngredientService.save(recipeIngredient);
            view = "redirect:/recipe/edit/"+id;
        }
        return view;
    }

    @GetMapping(value = "/{recipeId}/delete/{ingredientId}/{uomId}")
    public String deleteRecipe(
            @PathVariable("recipeId") Long recipeId,
            @PathVariable("ingredientId") Long ingredientId,
            @PathVariable("uomId") Long uomId,
            final RedirectAttributes redirectAttributes) {
        RecipeIngredientKey ringKey = new RecipeIngredientKey();
        ringKey.setRecipeId(recipeId);
        ringKey.setIngredientId(ingredientId);
        ringKey.setUomId(uomId);
        RecipeIngredient ring=this.recipeIngredientService.getByCompId(ringKey);
        if (ring != null) {
            try {
                this.recipeIngredientService.delete(ring);
                redirectAttributes.addFlashAttribute("success",true);
                redirectAttributes.addFlashAttribute("message","Deleted successfully: "+ring.getIngredient().getName());
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("success",false);
                redirectAttributes.addFlashAttribute("message","Can't delete due to DataIntegrity issue : "+ring.getIngredient().getName());
            }
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","RecipeIngredient not exists with id: "+ringKey);
        }
        return "redirect:/recipe/edit/"+recipeId;

    }

    @GetMapping(value = "/delete/{id}")
    public String deleteRecipe(
            @PathVariable("id") Long id,
            RedirectAttributes redirectAttributes) {
        Recipe recipe=this.recipeService.getRecipeById(id);

        if (recipe!=null) {
            try {
                this.recipeService.deleteRecipeById(id);
                redirectAttributes.addFlashAttribute("success",true);
                redirectAttributes.addFlashAttribute("message","Deleted successfully: "+recipe.getName());
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("success",false);
                redirectAttributes.addFlashAttribute("message","Can't delete due to DataIntegrity issue : "+recipe.getName());
            }
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Recipe not exists with id: "+id);
        }
        return "redirect:/recipe/all";
    }

}
