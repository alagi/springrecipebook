package hu.alagi.SpringRecipeBook.controllers.web;

import hu.alagi.SpringRecipeBook.entity.Ingredient;
import hu.alagi.SpringRecipeBook.entity.UnitOfMeasure;
import hu.alagi.SpringRecipeBook.service.UnitOfMeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/uom")
public class UnitOfMeasureController {

    private static final String FOLDER_UOM = "unit-of-measure/";

    private final UnitOfMeasureService unitOfMeasureService;

    public UnitOfMeasureController(@Autowired UnitOfMeasureService unitOfMeasureService) {
        this.unitOfMeasureService = unitOfMeasureService;
    }

    @GetMapping("/all")
    public String findAllUnitOfMeasure(Model model) {
        model.addAttribute("units", unitOfMeasureService.getAllCategories());
        model.addAttribute("numberOfUnits", unitOfMeasureService.countAll());
        return FOLDER_UOM+"units";
    }

    @GetMapping("/edit/{id}")
    public String updateForm(
            @PathVariable("id") Long id,
            Model model,
            final RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "upd");
        UnitOfMeasure uom = unitOfMeasureService.getById(id);

        if(uom!=null) {
            if (model.getAttribute("unit") == null) {
                model.addAttribute("unit", uom);
            }
            return FOLDER_UOM+"unit-form";
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Unit of Measure not exists with id: "+id);
        }
        return "redirect:/uom/all";
    }

    @PostMapping("/update/{id}")
    public String updateUnit(
            @PathVariable("id") Long id,
            @Valid @ModelAttribute("unit") UnitOfMeasure uom,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {
        uom.setId(id);

        String view;
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Update failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.unit", result);
            redirectAttributes.addFlashAttribute("unit", uom);
            view = "redirect:/uom/edit/"+id;
        } else {
            unitOfMeasureService.save(uom);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Updated successfully: "+uom.getAbbreviation());
            view = "redirect:/uom/all";
        }
        return view;
    }

    @GetMapping("/new")
    public String showNew(Model model) {
        model.addAttribute("action", "new");
        if (model.getAttribute("unit") ==null) {
            model.addAttribute("unit", new UnitOfMeasure());
        }
        return FOLDER_UOM+"unit-form";
    }

    @PostMapping("/add")
    public String addUnit(
            @Valid @ModelAttribute("unit") UnitOfMeasure uom,
            BindingResult result,
            final RedirectAttributes redirectAttributes) {
        String view;

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("success", false);
            redirectAttributes.addFlashAttribute("message", "Adding new failed!");
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.unit", result);
            redirectAttributes.addFlashAttribute("unit", uom);
            view = "redirect:/uom/new/";
        } else {
            this.unitOfMeasureService.save(uom);
            redirectAttributes.addFlashAttribute("success",true);
            redirectAttributes.addFlashAttribute("message","Added successfully: "+uom.getAbbreviation());
            view = "redirect:/uom/all";
        }
        return view;
    }

    @GetMapping("/delete/{id}")
    public String deleteUnit(
            @PathVariable("id") Long id,
            RedirectAttributes redirectAttributes) {
        UnitOfMeasure uom=this.unitOfMeasureService.getById(id);

        if (uom!=null) {
            try {
                this.unitOfMeasureService.delete(id);
                redirectAttributes.addFlashAttribute("success",true);
                redirectAttributes.addFlashAttribute("message","Deleted successfully: "+uom.getAbbreviation());
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("success",false);
                redirectAttributes.addFlashAttribute("message","Can't delete due to DataIntegrity issue : "+uom.getAbbreviation());
            }
        } else {
            redirectAttributes.addFlashAttribute("success",false);
            redirectAttributes.addFlashAttribute("message","Unit of Measure not exists with id: "+id);
        }
        return "redirect:/uom/all";
    }
}
