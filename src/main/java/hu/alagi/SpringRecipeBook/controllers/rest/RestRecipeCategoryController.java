package hu.alagi.SpringRecipeBook.controllers.rest;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.service.RecipeCategoryService;
import hu.alagi.SpringRecipeBook.service.RecipeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Log4j2
@RestController
@RequestMapping(value="/api/v1/recipe-categories")
public class RestRecipeCategoryController {
    final RecipeCategoryService recipeCategoryService;
    private final RecipeService recipeService;

    public RestRecipeCategoryController(@Autowired RecipeCategoryService recipeCategoryService, @Autowired RecipeService recipeService) {
        this.recipeCategoryService = recipeCategoryService;
        this.recipeService = recipeService;
    }

    @GetMapping(value={"/",""})
    public ResponseEntity<List<RecipeCategory>> getAll() {
        List<RecipeCategory> recipeCategoryList=recipeCategoryService.getAllCategories();
        log.info("REST: Requested all RecipeCategories");
        if (recipeCategoryList.isEmpty()) {
            log.info("REST: Get all items...empty - HttpStatus: {}", HttpStatus.NO_CONTENT.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        log.info("REST: Get all items... - HttpStatus: {}", HttpStatus.OK.toString());
        return new ResponseEntity<>(recipeCategoryList, HttpStatus.OK);
    }

    @GetMapping("/{recipeCategoryId}")
    public ResponseEntity<RecipeCategory> findCategoryById(@PathVariable Long recipeCategoryId) {
        RecipeCategory recipeCategory = recipeCategoryService.getById(recipeCategoryId);
        if (recipeCategory == null) {
            log.info("REST: Getting item with id: {} - HttpStatus: {}", recipeCategory, HttpStatus.NOT_FOUND.toString());
            throw new RecordNotFoundException("Record with id: "+recipeCategoryId+" not found");
            //return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("REST: Getting item with id: {} - HttpStatus: {}", recipeCategoryId, HttpStatus.OK.toString());
        return new ResponseEntity<>(recipeCategory, HttpStatus.OK);
    }

    @GetMapping("/{recipeCategoryId}/recipes")
    public ResponseEntity<List<Recipe>> findRecipesByCategory(@PathVariable Long recipeCategoryId) {
        RecipeCategory recipeCategory = recipeCategoryService.getById(recipeCategoryId);
        if (recipeCategory == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<Recipe> recipes = recipeService.getRecipesByRecipeCategoryId(recipeCategoryId);

        if (recipes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(recipes, HttpStatus.OK);
    }

    @RequestMapping(value={"","/"}, consumes = MediaType.APPLICATION_JSON_VALUE, method={RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<?> addRecipeCategory(@Valid @RequestBody RecipeCategory recipeCategory, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }

        RecipeCategory savedRecipeCategory = recipeCategoryService.save(recipeCategory);
        URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{recipeCategoryId}")
                .buildAndExpand(savedRecipeCategory.getId()).toUri();
        return ResponseEntity.created(location).header("Location", location.toString()).body(savedRecipeCategory);
    }

    @PutMapping(value="/{recipeCategoryId}")
    public ResponseEntity<?> updateCategory(@Valid @RequestBody RecipeCategory recipeCategory, @PathVariable Long recipeCategoryId, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }
        RecipeCategory rcat=this.recipeCategoryService.updateOrCreate(recipeCategory, recipeCategoryId);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .buildAndExpand(rcat.getId()).toUri();
        return ResponseEntity.ok().header("Location", location.toString()).body(rcat);
    }

    @DeleteMapping("/{recipeCategoryId}")
    ResponseEntity<Void> deleteRecipeCategory(@PathVariable Long recipeCategoryId) {
        RecipeCategory recipeCategory=recipeCategoryService.getById(recipeCategoryId);
        if (recipeCategory==null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        this.recipeCategoryService.delete(recipeCategoryId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value="/{recipeCategoryId}")
    public ResponseEntity<?> patchCategory(@PathVariable Long recipeCategoryId, @Valid @RequestBody RecipeCategory recipeCategory, Errors errors) {

        if (errors.hasFieldErrors("name") && recipeCategory.getName()!=null) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }
        RecipeCategory rcat=this.recipeCategoryService.patch(recipeCategory, recipeCategoryId);
        if (rcat!=null) {
            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .buildAndExpand(rcat.getId()).toUri();
            return ResponseEntity
                    .ok()
                    .header("Location", location.toString())
                    .body(rcat);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidationError handleException(Exception exception) {
        return new ValidationError(exception.getMessage());
    }
}
