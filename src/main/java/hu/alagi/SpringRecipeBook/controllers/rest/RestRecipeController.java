package hu.alagi.SpringRecipeBook.controllers.rest;

import hu.alagi.SpringRecipeBook.entity.Recipe;
import hu.alagi.SpringRecipeBook.entity.RecipeCategory;
import hu.alagi.SpringRecipeBook.entity.RecipeIngredient;
import hu.alagi.SpringRecipeBook.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/recipes",consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class RestRecipeController  {

    RecipeService recipeService;

    public RestRecipeController(@Autowired RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping({"/",""})
    public ResponseEntity<List<Recipe>> getAllRecipes() {
        List<Recipe> recipeList=recipeService.getAllRecipes();
        if (recipeList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(recipeList, HttpStatus.OK);
    }

    @GetMapping("/{recipeId}")
    public ResponseEntity<Recipe> findRecipeById(@PathVariable Long recipeId) {
        Recipe recipe = recipeService.getRecipeById(recipeId);
        if (recipe == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(recipe, HttpStatus.OK);
    }

    @GetMapping("/{recipeId}/ingredients")
    public ResponseEntity<List<RecipeIngredient>> getIngredientsByRecipe(@PathVariable Long recipeId) {
        Recipe recipe = recipeService.getRecipeById(recipeId);
        if (recipe == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (recipe.getRecipeIngredients().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(recipe.getRecipeIngredients(), HttpStatus.OK);
        }
    }




    @RequestMapping(value={"","/"}, consumes = MediaType.APPLICATION_JSON_VALUE, method={RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<?> addRecipe(@Valid @RequestBody Recipe recipe, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }
        recipeService.save(recipe);
        return new ResponseEntity<>(recipe, HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidationError handleException(Exception exception) {
        return new ValidationError(exception.getMessage());
    }

}
