package hu.alagi.SpringRecipeBook.config;

import hu.alagi.SpringRecipeBook.entity.security.UserRole;
import hu.alagi.SpringRecipeBook.security.UserEntityDetailsService;
import hu.alagi.SpringRecipeBook.security.UserEntityPasswordService;
import hu.alagi.SpringRecipeBook.security.mfa.CustomAuthenticationProvider;
import hu.alagi.SpringRecipeBook.security.mfa.CustomWebAuthenticationDetailsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UnAuthenticatedEndpointProvider unAuthenticatedEndpointProvider = new UnAuthenticatedEndpointProvider();
    private final UserEntityDetailsService userEntityDetailsService;
    private final UserEntityPasswordService userEntityPasswordService;
    private final CustomWebAuthenticationDetailsSource authenticationDetailsSource;

    public SecurityConfig(
            @Autowired UserEntityDetailsService userEntityDetailsService,
            @Autowired UserEntityPasswordService userEntityPasswordService,
            @Autowired CustomWebAuthenticationDetailsSource authenticationDetailsSource) {

        this.userEntityDetailsService = userEntityDetailsService;
        this.userEntityPasswordService = userEntityPasswordService;
        this.authenticationDetailsSource = authenticationDetailsSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .authorizeRequests()
                    .antMatchers(unAuthenticatedEndpointProvider.getUnAuthenticated())
                    .permitAll()
                    .antMatchers("/admin/**").hasRole(UserRole.ADMIN.name)
                    //.antMatchers("/recipe/**").hasAnyRole(UserRole.ADMIN.name, UserRole.USER.name)
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .defaultSuccessUrl("/index",true)
                    .authenticationDetailsSource(authenticationDetailsSource)
                    .and()
                .logout()
                    .permitAll()
                    .deleteCookies("JSESSIONID");


        http.headers().frameOptions().sameOrigin();
        http.csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider=new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsPasswordService(this.userEntityPasswordService);
        provider.setUserDetailsService(this.userEntityDetailsService);
        return provider;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(authProvider());
            //    .userDetailsService(userEntityDetailsService);
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        CustomAuthenticationProvider authProvider = new CustomAuthenticationProvider();
        authProvider.setUserDetailsService(this.userEntityDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }


}
