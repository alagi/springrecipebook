package hu.alagi.SpringRecipeBook.config;

public class UnAuthenticatedEndpointProvider {

        public static final String[] DEFAULT_UNAUTHENTICATED_ENDPOINTS = new String[] {
                "/configuration",
                "/swagger*/**",
                "/actuator/**",
                "/webjars/**",
                "/h2-console/**",
                "/resources/**",
                "/api/**"
        };

        public String[] getUnAuthenticated() {
            return DEFAULT_UNAUTHENTICATED_ENDPOINTS;
        }
}
